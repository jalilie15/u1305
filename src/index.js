const express = require('express');
const conectarBD=require("../config/db");
const cors=require('cors');
const app = express();

const port = 7000;

//enlazar la conexión con la base de datos
conectarBD();
app.use(cors());
app.use(express.json());
app.use('/api/productos', require ('../routes/productos')); 

// se muestra un mensaje en el navegador
app.get('/', (req,res) => {
res.send("bienvenido ya esta conectado su servidor en el navegador");
});




app.listen (port,()=> console.log('está conectado el servidor en el puerto',port));


