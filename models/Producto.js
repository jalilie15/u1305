const mongoose=require('mongoose');
//MODELO QUE DEBE SER IGUAL AL ESQUEMA DE LA BD
const productoShema= mongoose.Schema({
    nombre:{
        type:String,
        required:true
    },
    categoria:{
        type:String,
        required:true
    },
    precio:{
        type:Number,
        required:true
    },
    presentacion:{
        type:String,
        required:true
    },
    cantidad:{
        type:Number,
        required:true
    },
    precio_venta:{
        type:Number,
        required:true
    }

},{ versionKey: false });

module.exports=mongoose.model("Producto", productoShema);