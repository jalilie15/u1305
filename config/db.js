const mongoose = require('mongoose');
require("dotenv").config();

const conectarBD = ()=>{
    // conexión con mongodb
mongoose
.connect(process.env.MONGO_URL)
.then(()=> console.log("Está conectado al servidor MongoDB"))
.catch((err)=> console.error(err))
}
module.exports=conectarBD;
